'use strict';
module.exports = (sequelize, DataTypes) => {
  const Master_peserta_didik = sequelize.define('Master_peserta_didik', {
    peserta_didik_id: DataTypes.STRING,
    nama: DataTypes.STRING,
    nisn: DataTypes.STRING,
    nipd: DataTypes.STRING,
    jenis_kelamin: DataTypes.STRING,
    tempat_lahir: DataTypes.STRING,
    tgl_lahir: DataTypes.STRING,
    agama_id: DataTypes.STRING
  }, {
    freezeTableName: true,
    tableName: 'master_peserta_didik',
    timestamps: false
  });
  Master_peserta_didik.associate = function(models) {
    // associations can be defined here
  };
  return Master_peserta_didik;
};