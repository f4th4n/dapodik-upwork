'use strict';
module.exports = (sequelize, DataTypes) => {
  const Sekolah_data_rombel = sequelize.define('Sekolah_data_rombel', {
    rombongan_belajar_id: DataTypes.STRING,
    nama_rombel: DataTypes.STRING,
    master_tingkat_id: DataTypes.INTEGER,
    master_jurusan_id: DataTypes.INTEGER,
    sekolah_data_walas_id: DataTypes.INTEGER,
    sekolah_data_semester_id: DataTypes.INTEGER,
    tgl_insert: DataTypes.DATE
  }, {
    freezeTableName: true,
    tableName: 'sekolah_data_rombel',
    timestamps: false
  });
  Sekolah_data_rombel.associate = function(models) {
    // associations can be defined here
  };
  return Sekolah_data_rombel;
};