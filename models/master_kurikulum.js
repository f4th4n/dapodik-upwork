'use strict';
module.exports = (sequelize, DataTypes) => {
  const Master_kurikulum = sequelize.define('Master_kurikulum', {
    nama_kurikulum: DataTypes.STRING
  }, {
    freezeTableName: true,
    tableName: 'master_kurikulum',
    timestamps: false
  });
  Master_kurikulum.associate = function(models) {
    // associations can be defined here
  };
  return Master_kurikulum;
};
