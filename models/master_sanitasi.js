'use strict';
module.exports = (sequelize, DataTypes) => {
  const Master_sanitasi = sequelize.define('Master_sanitasi', {
    nama_provinsi: DataTypes.STRING,
  }, {
    freezeTableName: true,
    tableName: 'master_sanitasi',
    timestamps: false
  });
  Master_sanitasi.associate = function(models) {
    // associations can be defined here
  };
  return Master_sanitasi;
};