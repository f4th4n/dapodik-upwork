'use strict';
module.exports = (sequelize, DataTypes) => {
  const Sekolah_data_peserta_didik_rombel = sequelize.define('Sekolah_data_peserta_didik_rombel', {
    sekolah_data_peserta_register: DataTypes.INTEGER,
    sekolah_data_semester_id: DataTypes.INTEGER,
    sekolah_data_rombel_id: DataTypes.INTEGER,
    master_kurikulum_id: DataTypes.INTEGER,
    tgl_insert: DataTypes.DATE
  }, {
    freezeTableName: true,
    tableName: 'sekolah_data_peserta_didik_rombel',
    timestamps: false
  });
  Sekolah_data_peserta_didik_rombel.associate = function(models) {
    // associations can be defined here
  };
  return Sekolah_data_peserta_didik_rombel;
};