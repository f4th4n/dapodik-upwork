'use strict';
module.exports = (sequelize, DataTypes) => {
  const Master_bentuk_pendidikan = sequelize.define('Master_bentuk_pendidikan', {
    nama_bentuk_pendidikan: DataTypes.STRING
  }, {
    freezeTableName: true,
    tableName: 'master_bentuk_pendidikan',
    timestamps: false
  });
  Master_bentuk_pendidikan.associate = function(models) {
    // associations can be defined here
  };
  return Master_bentuk_pendidikan;
};