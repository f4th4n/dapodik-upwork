'use strict';
module.exports = (sequelize, DataTypes) => {
  const Master_tingkat = sequelize.define('Master_tingkat', {
    nama_tingkat: DataTypes.STRING
  }, {
    freezeTableName: true,
    tableName: 'master_tingkat',
    timestamps: false
  });
  Master_tingkat.associate = function(models) {
    // associations can be defined here
  };
  return Master_tingkat;
};