'use strict';
module.exports = (sequelize, DataTypes) => {
  const Sekolah_data_ptk_gtk = sequelize.define('Sekolah_data_ptk_gtk', {
    master_ptk_gtk_id: DataTypes.INTEGER,
    master_jenis_ptk_id: DataTypes.INTEGER,
    sekolah_data_semester: DataTypes.INTEGER,
    master_status_kepegawaian_id: DataTypes.INTEGER,
    tgl_surat_tugas: DataTypes.DATE,
    tgl_insert: DataTypes.DATE,
  }, {
    freezeTableName: true,
    tableName: 'sekolah_data_ptk_gtk',
    timestamps: false
  });
  Sekolah_data_ptk_gtk.associate = function(models) {
    // associations can be defined here
  };
  return Sekolah_data_ptk_gtk;
};