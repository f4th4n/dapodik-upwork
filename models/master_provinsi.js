'use strict';
module.exports = (sequelize, DataTypes) => {
  const Master_provinsi = sequelize.define('Master_provinsi', {
    nama_provinsi: DataTypes.STRING,
  }, {
    freezeTableName: true,
    tableName: 'master_provinsi',
    timestamps: false
  });
  Master_provinsi.associate = function(models) {
    // associations can be defined here
  };
  return Master_provinsi;
};