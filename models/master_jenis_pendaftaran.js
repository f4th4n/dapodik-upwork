'use strict';
module.exports = (sequelize, DataTypes) => {
  const Master_jenis_pendaftaran = sequelize.define('Master_jenis_pendaftaran', {
    nama_jenis_pendaftaran: DataTypes.STRING
  }, {
    freezeTableName: true,
    tableName: 'master_jenis_pendaftaran',
    timestamps: false
  });
  Master_jenis_pendaftaran.associate = function(models) {
    // associations can be defined here
  };
  return Master_jenis_pendaftaran;
};