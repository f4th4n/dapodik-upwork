'use strict';
module.exports = (sequelize, DataTypes) => {
  const Sekolah_data_profil = sequelize.define('Sekolah_data_profil', {
    sekolah_data_semester_id: DataTypes.INTEGER,
    kepsek_id: DataTypes.STRING,
    master_kurikulum: DataTypes.INTEGER,
    akreditasi: DataTypes.STRING,
    status_kepemilikan: DataTypes.STRING,
    sk_pendirian: DataTypes.STRING,
    tgl_pendirian: DataTypes.DATE,
    sk_izin_operasional: DataTypes.STRING,
    tgl_sk_izin_operasional: DataTypes.DATE,
    daya_listrik: DataTypes.INTEGER,
    status_bos: DataTypes.INTEGER,
    sertifikat_iso: DataTypes.STRING,
    operator_id: DataTypes.INTEGER,
    tgl_insert: DataTypes.DATE,
  }, {
    freezeTableName: true,
    tableName: 'sekolah_data_profil',
    timestamps: false
  });
  Sekolah_data_profil.associate = function(models) {
    // associations can be defined here
  };
  return Sekolah_data_profil;
};