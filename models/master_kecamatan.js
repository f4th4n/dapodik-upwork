'use strict';
module.exports = (sequelize, DataTypes) => {
  const Master_kecamatan = sequelize.define('Master_kecamatan', {
    nama_kecamatan: DataTypes.STRING,
    master_kabupaten_id: DataTypes.INTEGER
  }, {
    freezeTableName: true,
    tableName: 'master_kecamatan',
    timestamps: false
  });
  Master_kecamatan.associate = function(models) {
    // associations can be defined here
  };
  return Master_kecamatan;
};