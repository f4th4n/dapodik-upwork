'use strict';
module.exports = (sequelize, DataTypes) => {
  const Sekolah_sync = sequelize.define('Sekolah_sync', {
    sekolah_id: DataTypes.INTEGER,
    last_sync: { type: 'TIMESTAMP' },
    status_sync: DataTypes.INTEGER,
    created_data: DataTypes.DATE,
    semester_id: DataTypes.INTEGER
  }, {
    freezeTableName: true,
    tableName: 'sekolah_sync',
    timestamps: false
  });
  Sekolah_sync.associate = function(models) {
    // associations can be defined here
  };
  return Sekolah_sync;
};
