'use strict';
module.exports = (sequelize, DataTypes) => {
  const Sekolah_data_semester = sequelize.define('Sekolah_data_semester', {
    master_sekolah_id: DataTypes.INTEGER,
    master_semester_id: DataTypes.INTEGER,
    status: DataTypes.INTEGER,
    tgl_insert: DataTypes.DATE
  }, {
    freezeTableName: true,
    tableName: 'sekolah_data_semester',
    timestamps: false
  });
  Sekolah_data_semester.associate = function(models) {
    // associations can be defined here
  };
  return Sekolah_data_semester;
};