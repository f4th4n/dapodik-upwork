'use strict';
module.exports = (sequelize, DataTypes) => {
  const Master_jenis_sarpras = sequelize.define('Master_jenis_sarpras', {
    jenis_sarpras: DataTypes.STRING
  }, {
    freezeTableName: true,
    tableName: 'master_jenis_sarpras',
    timestamps: false
  });
  Master_jenis_sarpras.associate = function(models) {
    // associations can be defined here
  };
  return Master_jenis_sarpras;
};