'use strict';
module.exports = (sequelize, DataTypes) => {
  const sekolah_data_peserta_didik_register = sequelize.define('sekolah_data_peserta_didik_register', {
    master_peserta_didik_id: DataTypes.INTEGER,
    master_jenis_pendaftaran_id: DataTypes.INTEGER,
    tgl_masuk: DataTypes.DATE,
    status_peserta_didik: DataTypes.INTEGER,
    npsn: DataTypes.INTEGER,
    tgl_insert: DataTypes.DATE
  }, {
    freezeTableName: true,
    tableName: 'sekolah_data_peserta_didik_register',
    timestamps: false
  });
  sekolah_data_peserta_didik_register.associate = function(models) {
    // associations can be defined here
  };
  return sekolah_data_peserta_didik_register;
};