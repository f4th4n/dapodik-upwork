'use strict';
module.exports = (sequelize, DataTypes) => {
  const Master_jenis_ptk = sequelize.define('Master_jenis_ptk', {
    nama_jenis_ptk: DataTypes.STRING
  }, {
    freezeTableName: true,
    tableName: 'master_jenis_ptk',
    timestamps: false
  });
  Master_jenis_ptk.associate = function(models) {
    // associations can be defined here
  };
  return Master_jenis_ptk;
};