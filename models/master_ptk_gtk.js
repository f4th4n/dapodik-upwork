'use strict';
module.exports = (sequelize, DataTypes) => {
  const Master_ptk_gtk = sequelize.define('Master_ptk_gtk', {
    ptk_id: DataTypes.STRING,
    nik: DataTypes.STRING,
    nuptk: DataTypes.STRING,
    nip: DataTypes.STRING,
    nama: DataTypes.STRING,
    tempat_lahir: DataTypes.STRING,
    tgl_lahir: DataTypes.STRING,
    agama_id: DataTypes.STRING,
    jenis_kelamin: DataTypes.STRING,
    email: DataTypes.STRING
  }, {
    freezeTableName: true,
    tableName: 'master_ptk_gtk',
    timestamps: false
  });
  Master_ptk_gtk.associate = function(models) {
    // associations can be defined here
  };
  return Master_ptk_gtk;
};