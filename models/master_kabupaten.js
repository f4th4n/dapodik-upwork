'use strict';
module.exports = (sequelize, DataTypes) => {
  const Master_kabupaten = sequelize.define('Master_kabupaten', {
    nama_kabupaten: DataTypes.STRING,
    master_provinsi_id: DataTypes.INTEGER
  }, {
    freezeTableName: true,
    tableName: 'master_kabupaten',
    timestamps: false
  });
  Master_kabupaten.associate = function(models) {
    // associations can be defined here
  };
  return Master_kabupaten;
};