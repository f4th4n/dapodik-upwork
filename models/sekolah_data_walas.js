'use strict';
module.exports = (sequelize, DataTypes) => {
  const Sekolah_data_walas = sequelize.define('sekolah_data_walas', {
    master_ptk_gtk_id: DataTypes.INTEGER,
    semester_data_id: DataTypes.INTEGER,
    tgl_insert: new Date()
  }, {
    freezeTableName: true,
    tableName: 'sekolah_data_walas',
    timestamps: false
  });
  Sekolah_data_walas.associate = function(models) {
    // associations can be defined here
  };
  return Sekolah_data_walas;
};