'use strict';
module.exports = (sequelize, DataTypes) => {
  const Master_sekolah = sequelize.define('Master_sekolah', {
    npsn: DataTypes.INTEGER,
    nama: DataTypes.STRING,
    sekolah_id: DataTypes.STRING,
    sekolah_id_enkripsi: DataTypes.STRING,
    master_kecamatan_id: DataTypes.INTEGER,
    master_bentuk_pendidikan_id: DataTypes.INTEGER,
    status_sekolah: DataTypes.INTEGER,
    waktu_penyelanggaraan: DataTypes.STRING,
    desa: DataTypes.STRING,
    dusun: DataTypes.STRING,
    rt: DataTypes.STRING,
    rw: DataTypes.STRING,
    koordinat: DataTypes.STRING,
    sumber_listrik: DataTypes.STRING,
    luas_tanah: DataTypes.INTEGER,
    akses_internet: DataTypes.STRING,
  }, {
    freezeTableName: true,
    tableName: 'master_sekolah',
    timestamps: false
  });
  Master_sekolah.associate = function(models) {
    // associations can be defined here
  };
  return Master_sekolah;
};