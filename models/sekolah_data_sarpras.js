'use strict';
module.exports = (sequelize, DataTypes) => {
  const Sekolah_data_sarpras = sequelize.define('Sekolah_data_sarpras', {
    sekolah_data_semester_id: DataTypes.STRING,
    jenis_sarpras_id: DataTypes.INTEGER,
    jumlah: DataTypes.INTEGER,
    tgl_insert: DataTypes.DATE
  }, {
    freezeTableName: true,
    tableName: 'sekolah_data_sarpras',
    timestamps: false
  });
  Sekolah_data_sarpras.associate = function(models) {
    // associations can be defined here
  };
  return Sekolah_data_sarpras;
};