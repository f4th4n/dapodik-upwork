'use strict';
module.exports = (sequelize, DataTypes) => {
  const Master_jurusan = sequelize.define('Master_jurusan', {
    nama_jurusan: DataTypes.STRING
  }, {
    freezeTableName: true,
    tableName: 'master_jurusan',
    timestamps: false
  });
  Master_jurusan.associate = function(models) {
    // associations can be defined here
  };
  return Master_jurusan;
};