'use strict';
module.exports = (sequelize, DataTypes) => {
  const Master_semester = sequelize.define('Master_semester', {
    nama_semester: DataTypes.INTEGER,
    tahun_pelajaran: DataTypes.STRING
  }, {
    freezeTableName: true,
    tableName: 'master_semester',
    timestamps: false
  });
  Master_semester.associate = function(models) {
    // associations can be defined here
  };
  return Master_semester;
};