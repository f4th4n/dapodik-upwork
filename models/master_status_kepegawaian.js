'use strict';
module.exports = (sequelize, DataTypes) => {
  const Master_status_kepegawaian = sequelize.define('Master_status_kepegawaian', {
    nama_status_kepegawaian: DataTypes.STRING
  }, {
    freezeTableName: true,
    tableName: 'master_status_kepegawaian',
    timestamps: false
  });
  Master_status_kepegawaian.associate = function(models) {
    // associations can be defined here
  };
  return Master_status_kepegawaian;
};