const { Sequelize, sequelize } = require('./models')
const fs = require('fs').promises
const sekolahDataPesertaDidikRegister = require('./src/sekolah_data_peserta_didik_register')
const masterSemester = require('./src/master_semester')
const masterPtkGtK = require('./src/master_ptk_gtk')
const masterPesertaDidik = require('./src/master_peserta_didik')
const masterKurikulum = require('./src/master_kurikulum')
const masterJenisSarpras = require('./src/master_jenis_sarpras')
const masterJenisPtk = require('./src/master_jenis_ptk')
const masterJenisPendaftaran = require('./src/master_jenis_pendaftaran')
const masterJurusan = require('./src/master_jurusan')
const masterStatusKepegawaian = require('./src/master_status_kepegawaian')
const masterTingkat = require('./src/master_tingkat')
const masterSanitasi = require('./src/master_sanitasi')
const masterProvinsi = require('./src/master_provinsi')
const masterKabupaten = require('./src/master_kabupaten')
const masterKecamatan = require('./src/master_kecamatan')
const masterSekolah = require('./src/master_sekolah')
const masterBentukPendidikan = require('./src/master_bentuk_pendidikan')
const sekolahDataSemester = require('./src/sekolah_data_semester')
const sekolahDataWalas = require('./src/sekolah_data_walas')
const sekolahDataRombel = require('./src/sekolah_data_rombel')
const sekolahDataPesertaDidikRombel = require('./src/sekolah_data_peserta_didik_rombel')
const sekolahDataProfil = require('./src/sekolah_data_profil')
const sekolahDataPtkGtk = require('./src/sekolah_data_ptk_gtk')
const sekolahSync = require('./src/sekolah_sync')

process.on('unhandledRejection', function(err) {
  console.log(err)
})

const importExec = async () => {
  try {
    console.log('import masterSemester [1/24]')
    await masterSemester.importData()
    console.log('import masterPtkGtK [2/24]')
    await masterPtkGtK.importData()
    console.log('import masterPesertaDidik [3/24]')
    await masterPesertaDidik.importData()
    console.log('import masterKurikulum [4/24]')
    await masterKurikulum.importData()
    console.log('import masterJenisSarpras [5/24]')
    await masterJenisSarpras.importData()
    console.log('import masterJenisPtk [6/24]')
    await masterJenisPtk.importData()
    console.log('import masterJenisPendaftaran [7/24]')
    await masterJenisPendaftaran.importData()
    console.log('import masterJurusan [8/24]')
    await masterJurusan.importData()
    console.log('import masterStatusKepegawaian [9/24]')
    await masterStatusKepegawaian.importData()
    console.log('import masterTingkat [10/24]')
    await masterTingkat.importData()
    console.log('import masterSanitasi [11/24]')
    await masterSanitasi.importData()
    console.log('import masterProvinsi [12/24]')
    await masterProvinsi.importData()
    console.log('import masterKabupaten [13/24]')
    await masterKabupaten.importData()
    console.log('import masterKecamatan [14/24]')
    await masterKecamatan.importData()
    console.log('import masterBentukPendidikan [15/24]')
    await masterBentukPendidikan.importData()
    console.log('import masterSekolah [16/24]')
    await masterSekolah.importData()
    console.log('import sekolahDataPesertaDidikRegister [17/24]')
    await sekolahDataPesertaDidikRegister.importData()
    console.log('import sekolahDataSemester [18/24]')
    await sekolahDataSemester.importData()
    console.log('import sekolahDataWalas [19/24]')
    await sekolahDataWalas.importData()
    console.log('import sekolahDataRombel [20/24]')
    await sekolahDataRombel.importData()
    console.log('import sekolahDataPesertaDidikRombel [21/24]')
    await sekolahDataPesertaDidikRombel.importData()
    console.log('import sekolahDataProfil [22/24]')
    await sekolahDataProfil.importData()
    console.log('import sekolahDataPtkGtk [23/24]')
    await sekolahDataPtkGtk.importData()
    console.log('import sekolahSync [24/24]')
    await sekolahSync.importData()
  } catch(e) {
    console.log('e', e)
    await fs.writeFile('log.txt', JSON.stringify(e))
  }
}

const resetExec = async () => {
  try {
    const tables = [
      'master_bentuk_pendidikan',
      'master_jenis_pendaftaran',
      'master_jenis_ptk',
      'master_jenis_sarpras',
      'master_jurusan',
      'master_kabupaten',
      'master_kecamatan',
      'master_kurikulum',
      'master_peserta_didik',
      'master_provinsi',
      'master_ptk_gtk',
      'master_sanitasi',
      'master_sekolah',
      'master_semester',
      'master_status_kepegawaian',
      'master_tingkat',
    ]
    for(let table of tables) {
      let model = sequelize.import('./models/' + table)
      await model.destroy({where: {}, truncate: true})
    }
  } catch(e) {
    console.log('e', e)
  }
}

const resetAll = async () => {
  try {
    const tables = [
      //'sekolah_data_peserta_didik_register',
      //'sekolah_data_peserta_didik_rombel',
      //'sekolah_data_profil',
      //'sekolah_data_ptk_gtk',
      //'sekolah_data_rombel',
      //'sekolah_data_sanitasi',
      //'sekolah_data_sarpras',
      'sekolah_data_semester',
      //'sekolah_data_walas',
      //'sekolah_sync'
    ]
    for(let table of tables) {
      let model = sequelize.import('./models/' + table)
      await model.destroy({where: {}, truncate: true})
    }
  } catch(e) {
    console.log('e', e)
  }
}

if(process.argv[2] === 'import') {
  importExec().then(process.exit)
} else if(process.argv[2] === 'reset') {
  resetExec().then(process.exit)
} else if(process.argv[2] === 'reset-all') {
  resetExec().then(process.exit)
} else {
  console.log('npm run import|reset|reset-all')
}
