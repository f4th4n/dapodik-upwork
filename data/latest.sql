/*
 Navicat MySQL Data Transfer

 Source Server         : SGDB
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : SG-DBMasterDapodik-1173-master.servers.mongodirector.com:3306
 Source Schema         : DBDapodikWEB

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 04/09/2019 13:25:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for master_bentuk_pendidikan
-- ----------------------------
DROP TABLE IF EXISTS `master_bentuk_pendidikan`;
CREATE TABLE `master_bentuk_pendidikan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_bentuk_pendidikan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'SD/SMP/SMA/SMK/SLB diambil dari data JSON\r\nsma-3-030105.json',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of master_bentuk_pendidikan
-- ----------------------------

-- ----------------------------
-- Table structure for master_jenis_pendaftaran
-- ----------------------------
DROP TABLE IF EXISTS `master_jenis_pendaftaran`;
CREATE TABLE `master_jenis_pendaftaran`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis_pendaftaran` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'diambil dari jenis_pendaftaran_string di file getPesertaDidik.json',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of master_jenis_pendaftaran
-- ----------------------------

-- ----------------------------
-- Table structure for master_jenis_ptk
-- ----------------------------
DROP TABLE IF EXISTS `master_jenis_ptk`;
CREATE TABLE `master_jenis_ptk`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama_jenis_ptk` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'Guru Mapel, Guru TIK, Tenaga Administrasi Sekolah -> diambilkan dari getGTK.json',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for master_jenis_sarpras
-- ----------------------------
DROP TABLE IF EXISTS `master_jenis_sarpras`;
CREATE TABLE `master_jenis_sarpras`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `jenis_sarpras` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'data_sarpras dari file rekapitulasi.json',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of master_jenis_sarpras
-- ----------------------------
INSERT INTO `master_jenis_sarpras` VALUES (1, 'Ruang Kelas');
INSERT INTO `master_jenis_sarpras` VALUES (2, 'Ruang Laboratorium');
INSERT INTO `master_jenis_sarpras` VALUES (3, 'Ruang Perpustakaan');

-- ----------------------------
-- Table structure for master_jurusan
-- ----------------------------
DROP TABLE IF EXISTS `master_jurusan`;
CREATE TABLE `master_jurusan`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nama_jurusan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'jurusan_id_string dari file getRombonganBelajar.json',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for master_kabupaten
-- ----------------------------
DROP TABLE IF EXISTS `master_kabupaten`;
CREATE TABLE `master_kabupaten`  (
  `id` int(10) NOT NULL COMMENT 'kode_wilayah dari file 1-030000.json',
  `nama_kabupaten` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'nama dari file 1-030000.json',
  `master_provinsi_id` int(10) NULL DEFAULT NULL COMMENT 'kode_wilayah_induk_provinsi dari file 1-030000.json',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for master_kecamatan
-- ----------------------------
DROP TABLE IF EXISTS `master_kecamatan`;
CREATE TABLE `master_kecamatan`  (
  `id` int(10) NOT NULL COMMENT 'kode_wilayah dari 2-030100.json',
  `nama_kecamatan` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'nama dari 2-030100.json',
  `master_kabupaten_id` int(10) NULL DEFAULT NULL COMMENT 'kode_wilayah_induk_kabupaten dari 2-030100.json',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for master_kurikulum
-- ----------------------------
DROP TABLE IF EXISTS `master_kurikulum`;
CREATE TABLE `master_kurikulum`  (
  `id` int(5) NOT NULL COMMENT 'kurikulum_id dari getRombonganBelajar.json',
  `nama_kurikulum` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'kurikulum_id_str dari getRombonganBelajar.json',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for master_peserta_didik
-- ----------------------------
DROP TABLE IF EXISTS `master_peserta_didik`;
CREATE TABLE `master_peserta_didik`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'semua kolom diambil dari File getPesertadidik.json',
  `nisn` int(10) NULL DEFAULT NULL,
  `nipd` int(10) NULL DEFAULT NULL,
  `jenis_kelamin` enum('L','P') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tempat_lahir` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_lahir` date NULL DEFAULT NULL,
  `agama_id` int(2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for master_provinsi
-- ----------------------------
DROP TABLE IF EXISTS `master_provinsi`;
CREATE TABLE `master_provinsi`  (
  `id` int(10) NOT NULL COMMENT 'kode wilayah di file sp.json',
  `nama_provinsi` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'nama di file sp.json',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for master_ptk_gtk
-- ----------------------------
DROP TABLE IF EXISTS `master_ptk_gtk`;
CREATE TABLE `master_ptk_gtk`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nik` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'semua diambil dari file getGTK.json',
  `nuptk` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nip` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tempat_lahir` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_lahir` date NULL DEFAULT NULL,
  `agama_id` int(2) NULL DEFAULT NULL,
  `jenis_kelamin` enum('L','P') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for master_sanitasi
-- ----------------------------
DROP TABLE IF EXISTS `master_sanitasi`;
CREATE TABLE `master_sanitasi`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nama_sanitasi` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'data_sanitasi di school/{npsn}/rekapitulasi.json',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of master_sanitasi
-- ----------------------------
INSERT INTO `master_sanitasi` VALUES (1, 'Kecukupan Air');
INSERT INTO `master_sanitasi` VALUES (2, 'Sekolah memproses air sendiri');

-- ----------------------------
-- Table structure for master_sekolah
-- ----------------------------
DROP TABLE IF EXISTS `master_sekolah`;
CREATE TABLE `master_sekolah`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `npsn` int(8) NULL DEFAULT NULL COMMENT 'diambil dari school/{npsn}/profil.json',
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sekolah_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sekolah_id_enkripsi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `master_kecamatan_id` int(10) NULL DEFAULT NULL,
  `master_bentuk_pendidikan_id` int(2) NULL DEFAULT NULL,
  `status_sekolah` int(1) NULL DEFAULT NULL COMMENT '1=negri / 2=swasta',
  `waktu_penyelanggaraan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'pagi / sore',
  `desa` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `dusun` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `rt` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `rw` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `koordinat` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sumber_listrik` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `luas_tanah` int(5) NULL DEFAULT NULL,
  `akses_internet` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for master_semester
-- ----------------------------
DROP TABLE IF EXISTS `master_semester`;
CREATE TABLE `master_semester`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nama_semester` int(1) NULL DEFAULT NULL COMMENT '0 = Genap, 1 = Ganjil',
  `tahun_pelajaran` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'misal 2019/2020',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for master_status_kepegawaian
-- ----------------------------
DROP TABLE IF EXISTS `master_status_kepegawaian`;
CREATE TABLE `master_status_kepegawaian`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nama_status_kepegawaian` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'diambil dari getGtk.json',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for master_tingkat
-- ----------------------------
DROP TABLE IF EXISTS `master_tingkat`;
CREATE TABLE `master_tingkat`  (
  `id` int(2) NOT NULL COMMENT 'diambil dari getPesertadidik.json',
  `nama_tingkat` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'kelas 1 SD = tingkat 1, kelas 1 SMP = tingkat 7, kelas 1 SMK/A = tingkat 10',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sekolah_data_peserta_didik_register
-- ----------------------------
DROP TABLE IF EXISTS `sekolah_data_peserta_didik_register`;
CREATE TABLE `sekolah_data_peserta_didik_register`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `master_peserta_didik_id` int(10) NULL DEFAULT NULL,
  `master_jenis_pendaftaran_id` int(2) NULL DEFAULT NULL,
  `tgl_masuk` date NULL DEFAULT NULL,
  `status_peserta_didik` int(2) NULL DEFAULT NULL COMMENT '1=Aktif,0 = Tidak Aktif',
  `npsn` int(15) NULL DEFAULT NULL,
  `tgl_insert` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sekolah_data_peserta_didik_rombel
-- ----------------------------
DROP TABLE IF EXISTS `sekolah_data_peserta_didik_rombel`;
CREATE TABLE `sekolah_data_peserta_didik_rombel`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sekolah_data_peserta_register` int(10) NULL DEFAULT NULL,
  `sekolah_data_semester_id` int(10) NULL DEFAULT NULL,
  `sekolah_data_rombel_id` int(10) NULL DEFAULT NULL,
  `master_kurikulum_id` int(10) NULL DEFAULT NULL,
  `tgl_insert` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sekolah_data_profil
-- ----------------------------
DROP TABLE IF EXISTS `sekolah_data_profil`;
CREATE TABLE `sekolah_data_profil`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sekolah_data_semester_id` int(10) NULL DEFAULT NULL,
  `kepsek_id` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'diambilkan dari table master_ptk_gtk',
  `master_kurikulum_id` int(3) NULL DEFAULT NULL,
  `akreditasi` int(5) NULL DEFAULT NULL,
  `status_kepemilikan` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sk_pendirian` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_pendirian` date NULL DEFAULT NULL,
  `sk_izin_operasional` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_sk_izin_operasional` date NULL DEFAULT NULL,
  `daya_listrik` int(5) NULL DEFAULT NULL,
  `status_bos` int(2) NULL DEFAULT NULL COMMENT '1 = Menerima, 0 = Tidak Menerima',
  `sertifikat_iso` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `operator_id` int(10) NULL DEFAULT NULL COMMENT 'diambilkan dari table master_ptk_gtk',
  `tgl_insert` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sekolah_data_ptk_gtk
-- ----------------------------
DROP TABLE IF EXISTS `sekolah_data_ptk_gtk`;
CREATE TABLE `sekolah_data_ptk_gtk`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `master_ptk_gtk_id` int(10) NULL DEFAULT NULL,
  `master_jenis_ptk_id` int(10) NULL DEFAULT NULL,
  `sekolah_data_semester` int(10) NULL DEFAULT NULL,
  `master_status_kepegawaian_id` int(10) NULL DEFAULT NULL,
  `tgl_surat_tugas` date NULL DEFAULT NULL,
  `tgl_insert` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sekolah_data_rombel
-- ----------------------------
DROP TABLE IF EXISTS `sekolah_data_rombel`;
CREATE TABLE `sekolah_data_rombel`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_rombel` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `master_tingkat_id` int(2) NULL DEFAULT NULL,
  `master_jurusan_id` int(50) NULL DEFAULT NULL,
  `sekolah_data_walas_id` int(11) NULL DEFAULT NULL,
  `sekolah_data_semester_id` int(5) NULL DEFAULT NULL,
  `tgl_insert` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sekolah_data_sanitasi
-- ----------------------------
DROP TABLE IF EXISTS `sekolah_data_sanitasi`;
CREATE TABLE `sekolah_data_sanitasi`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `sekolah_data_semester_id` int(5) NULL DEFAULT NULL,
  `master_sanitasi_id` int(5) NULL DEFAULT NULL,
  `uraian` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_insert` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sekolah_data_sarpras
-- ----------------------------
DROP TABLE IF EXISTS `sekolah_data_sarpras`;
CREATE TABLE `sekolah_data_sarpras`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sekolah_data_semester_id` int(10) NULL DEFAULT NULL,
  `jenis_sarpras_id` int(10) NULL DEFAULT NULL,
  `jumlah` int(5) NULL DEFAULT NULL,
  `tgl_insert` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sekolah_data_semester
-- ----------------------------
DROP TABLE IF EXISTS `sekolah_data_semester`;
CREATE TABLE `sekolah_data_semester`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `master_sekolah_id` int(10) NULL DEFAULT NULL,
  `master_semester_id` int(3) NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT NULL COMMENT '1=Aktif, 0 = Tidak Aktif',
  `tgl_insert` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sekolah_data_walas
-- ----------------------------
DROP TABLE IF EXISTS `sekolah_data_walas`;
CREATE TABLE `sekolah_data_walas`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `master_ptk_gtk_id` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `semester_data_id` int(10) NULL DEFAULT NULL,
  `tgl_insert` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sekolah_sync
-- ----------------------------
DROP TABLE IF EXISTS `sekolah_sync`;
CREATE TABLE `sekolah_sync`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sekolah_id` int(10) NULL DEFAULT NULL,
  `last_sync` datetime(0) NULL DEFAULT NULL,
  `status_sync` int(5) NULL DEFAULT NULL,
  `created_data` datetime(0) NULL DEFAULT NULL,
  `semester_id` int(5) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;


    ALTER TABLE `master_peserta_didik` ADD `peserta_didik_id` VARCHAR(64) NOT NULL AFTER `id`;
    ALTER TABLE `master_ptk_gtk` ADD `ptk_id` VARCHAR(64) NOT NULL AFTER `id`;
    ALTER TABLE `sekolah_data_rombel` ADD `rombongan_belajar_id` VARCHAR(64) NOT NULL AFTER `id`;
    ALTER TABLE `sekolah_data_profil` CHANGE `akreditasi` `akreditasi` VARCHAR(8) NULL DEFAULT NULL;
    ALTER TABLE `master_kurikulum` CHANGE `id` `id` INT(5) NOT NULL AUTO_INCREMENT COMMENT 'kurikulum_id dari getRombonganBelajar.json';
ALTER TABLE `master_provinsi` CHANGE `id` `id` INT(10) NOT NULL AUTO_INCREMENT COMMENT 'kode wilayah di file sp.json';

