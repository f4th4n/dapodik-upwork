(done) master_bentuk_pendidikan
(done) master_jenis_pendaftaran
(done) master_jenis_ptk
(done) master_jenis_sarpras
(done) master_jurusan
(done) master_kabupaten
(done) master_kecamatan
(done) master_kurikulum
(done) master_peserta_didik
(done) master_provinsi
(done) master_ptk_gtk
(done) master_sanitasi
(done) master_sekolah
(done) master_semester
(done) master_status_kepegawaian
(done) master_tingkat
(done) sekolah_data_peserta_didik_register
(done -) sekolah_data_peserta_didik_rombel
(done -) sekolah_data_profil
(done) sekolah_data_ptk_gtk
(done) sekolah_data_rombel
(todo) sekolah_data_sanitasi
(done) sekolah_data_sarpras
(done) sekolah_data_semester
(done) sekolah_data_walas
(todo) sekolah_sync

addition:
  create table agama: id, nama_agama
    ALTER TABLE `master_peserta_didik` ADD `peserta_didik_id` VARCHAR(64) NOT NULL AFTER `id`;
    ALTER TABLE `master_ptk_gtk` ADD `ptk_id` VARCHAR(64) NOT NULL AFTER `id`;
    ALTER TABLE `sekolah_data_rombel` ADD `rombongan_belajar_id` VARCHAR(64) NOT NULL AFTER `id`;
    ALTER TABLE `sekolah_data_profil` CHANGE `master_kurikulum_id` `master_kurikulum` VARCHAR(64) NULL DEFAULT NULL, CHANGE `akreditasi` `akreditasi` VARCHAR(8) NULL DEFAULT NULL;


cp models/master_status_kepegawaian.js models/sekolah_data_rombel.js
cp src/master_status_kepegawaian.js src/sekolah_data_rombel.js

  //await sequelize.import('../models/master_bentuk_pendidikan').destroy({where: {}, truncate: true})
