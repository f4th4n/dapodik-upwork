const config = require('../config/config')
const { Sequelize, sequelize } = require('../models')
const glob = require('glob-promise')
const util = require('util')
const _ = require('lodash')

const importData = async () => {
  const files = await glob(config.dataPath + '/**/getGTK.json')
  var rows = []
  var idCounter = 1

  for(let file of files) {
    const content = require('../' + file)
    for(let line of content.rows) {
      const row = {
        ptk_id: line.ptk_id,
        nik: line.nik,
        nuptk: line.nuptk,
        nip: line.nip,
        nama: line.nama,
        tempat_lahir: line.tempat_lahir,
        tgl_lahir: line.tanggal_lahir,
        agama_id: line.agama_id,
        jenis_kelamin: line.jenis_kelamin,
        email: line.email
      }
      rows.push(row)
    }
  }

  rows = _.uniqBy(rows, 'nik')
  const model = sequelize.import('../models/master_ptk_gtk')
  for(let row of rows) {
    const where = { nik: row.nik }

    const isExist = Boolean((await model.findAll({
      attributes: [[sequelize.fn('COUNT', sequelize.col('id')), 'count']],
      where
    }))[0].dataValues.count)
    if(isExist) continue

    await model.create(row, { ignoreDuplicates: true })
  }
}

module.exports.importData = importData