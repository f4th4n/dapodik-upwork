const config = require('../config/config')
const { Sequelize, sequelize } = require('../models')
const glob = require('glob-promise')
const util = require('util')
const getKecamatanIdByKodeWilayahInduk = require('./helper/get_kecamatan_id_by_kode_wilayah_induk')
const getBentukPendidikanIdByNama = require('./helper/get_bentuk_pendidikan_id_by_nama')
const getProfilByNPSN = require('./helper/get_profil_by_npsn')
const getKontakByNPSN = require('./helper/get_kontak_by_npsn')
const getSemesterId = require('./helper/get_semester_id')
const getSekolahDataSemesterId = require('./helper/get_sekolah_data_semester_id')

const importData = async () => {
  const files = await glob(config.dataPath + '/+(sd|slb|smp|sma|smk)-*')
  var rows = []

  for(let file of files) {
    const content = require('../' + file)
    for(let line of content) {
      const sekolah_data_semester_id = await getSekolahDataSemesterId(config.dataPath + '/' + line.npsn + '/profil.json')
      const get_sekolah_id = async (line) => {
        const model = sequelize.import('../models/master_sekolah')
        const row = await model.findOne({ where: { sekolah_id: line.sekolah_id }})

        if(!row) return null
        return row.id
      }

      const getLastSync = (line) => {
        if(!line || !line.sinkron_terakhir) return null

        // line e.g 02 September 2019 08:20:50.743
        const lastSync = new Date(line.sinkron_terakhir.replace(' pkl', ''))
        return lastSync
      }

      const row = {
        sekolah_id: await get_sekolah_id(line),
        last_sync: getLastSync(line),
        status_sync: 1,
        created_data: new Date(),
        semester_id: sekolah_data_semester_id
      }

      rows.push(row)
    }
  }

  const model = sequelize.import('../models/sekolah_sync')
  for(let row of rows) {
    const where = {
      sekolah_id: row.sekolah_id,
      semester_id: row.semester_id
    }

    const isExist = Boolean((await model.findAll({
      attributes: [[sequelize.fn('COUNT', sequelize.col('id')), 'count']],
      where
    }))[0].dataValues.count)
    if(isExist) continue

    await model.create(row, { ignoreDuplicates: true })
  }
}

module.exports.importData = importData
