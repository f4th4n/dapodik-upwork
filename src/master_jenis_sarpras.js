const config = require('../config/config')
const { Sequelize, sequelize } = require('../models')
const glob = require('glob-promise')
const util = require('util')
const MasterJenisSarpras = sequelize.import('../models/master_jenis_sarpras')
const getSekolahDataSemesterId = require('./helper/get_sekolah_data_semester_id')

const importData = async () => {
  const files = await glob(config.dataPath + '/**/rekapitulasi.json')
  var rows = []

  for(let file of files) {
    const content = require('../' + file)
    const sekolahDataSemesterId = await getSekolahDataSemesterId(file)
    var sarprasLab, row

    sarprasLab = await MasterJenisSarpras.findOne({ where: {jenis_sarpras: 'Ruang Laboratorium'} })
    row = {
      sekolah_data_semester_id: sekolahDataSemesterId,
      jenis_sarpras_id: (sarprasLab ? sarprasLab.id : null),
      jumlah: content.rekapitulasi.data_sarpras.jml_lab,
      tgl_insert: new Date()
    }
    rows.push(row)

    sarprasLab = await MasterJenisSarpras.findOne({ where: {jenis_sarpras: 'Ruang Perpustakaan'} })
    row = {
      sekolah_data_semester_id: sekolahDataSemesterId,
      jenis_sarpras_id: sarprasLab.id,
      jumlah: content.rekapitulasi.data_sarpras.jml_perpus,
      tgl_insert: new Date()
    }
    rows.push(row)
  }

  const model = sequelize.import('../models/sekolah_data_sarpras')
  for(let row of rows) {
    const where = {
      sekolah_data_semester_id: row.sekolah_data_semester_id,
      jenis_sarpras_id: row.jenis_sarpras_id
    }

    const isExist = Boolean((await model.findAll({
      attributes: [[sequelize.fn('COUNT', sequelize.col('id')), 'count']],
      where
    }))[0].dataValues.count)
    if(isExist) continue

    await model.create(row, { ignoreDuplicates: true })
  }
}

module.exports.importData = importData
