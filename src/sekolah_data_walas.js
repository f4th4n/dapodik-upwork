const config = require('../config/config')
const { Sequelize, sequelize } = require('../models')
const glob = require('glob-promise')
const util = require('util')
const _ = require('lodash')
const getPtkGtKId = require('./helper/get_ptk_gtk_id')
const getSemesterId = require('./helper/get_semester_id')

const importData = async () => {
  const files = await glob(config.dataPath + '/**/getRombel.json')
  var rows = []
  var idCounter = 1

  for(let file of files) {
    const content = require('../' + file)
    for(let line of content.rows) {
      const master_ptk_gtk_id = await getPtkGtKId(line.ptk_id)
      const semester_data_id = await getSemesterId(line.semester_id)

      const row = {
        master_ptk_gtk_id,
        semester_data_id,
        tgl_insert: new Date()
      }
      rows.push(row)
    }
  }

  const model = sequelize.import('../models/sekolah_data_walas')
  for(let row of rows) {
    const where = {
      master_ptk_gtk_id: row.master_ptk_gtk_id,
      semester_data_id: row.semester_data_id
    }

    const isExist = Boolean((await model.findAll({
      attributes: [[sequelize.fn('COUNT', sequelize.col('id')), 'count']],
      where
    }))[0].dataValues.count)
    if(isExist) continue

    await model.create(row, { ignoreDuplicates: true })
  }
}

module.exports.importData = importData