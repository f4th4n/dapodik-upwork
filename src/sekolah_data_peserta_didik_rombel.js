const config = require('../config/config')
const { Sequelize, sequelize } = require('../models')
const glob = require('glob-promise')
const util = require('util')
const _ = require('lodash')
const getSemesterId = require('./helper/get_semester_id')
const getRombelId = require('./helper/get_rombel_id')

const importData = async () => {
  const files = await glob(config.dataPath + '/**/getRombel.json')
  var rows = []
  var idCounter = 1

  for(let file of files) {
    const content = require('../' + file)
    for(let line of content.rows) {
      const sekolah_data_semester_id = await getSemesterId(line.semester_id)
      const sekolah_data_rombel_id = await getRombelId(line.rombongan_belajar_id)

      const row = {
        sekolah_data_peserta_register: 1, // TODO
        sekolah_data_semester_id,
        sekolah_data_rombel_id,
        master_kurikulum_id: line.kurikulum_id,
        tgl_insert: new Date()
      }
      rows.push(row)
    }
  }

  const model = sequelize.import('../models/sekolah_data_peserta_didik_rombel')
  for(let row of rows) {
    const where = {
      sekolah_data_peserta_register: row.sekolah_data_peserta_register,
      sekolah_data_semester_id: row.sekolah_data_semester_id,
      sekolah_data_rombel_id: row.sekolah_data_rombel_id,
      master_kurikulum_id: row.master_kurikulum_id
    }

    const isExist = Boolean((await model.findAll({
      attributes: [[sequelize.fn('COUNT', sequelize.col('id')), 'count']],
      where
    }))[0].dataValues.count)
    if(isExist) continue

    await model.create(row, { ignoreDuplicates: true })
  }
}

module.exports.importData = importData