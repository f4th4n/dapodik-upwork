const config = require('../config/config')
const { Sequelize, sequelize } = require('../models')
const glob = require('glob-promise')
const util = require('util')
const _ = require('lodash')
const getSekolahDataSemesterId = require('./helper/get_sekolah_data_semester_id')
const path = require('path')
const getMasterKurikulumId = require('./helper/get_master_kurikulum_id_by_master_kurikulum')

const importData = async () => {
  const files = await glob(config.dataPath + '/**/profil.json')
  var rows = []

  for(let file of files) {
    const content = require('../' + file)
    const sekolah_data_semester_id = await getSekolahDataSemesterId(file)
    const getKepsekId = async () => {
      const npsn = path.basename(path.dirname(file))
    }

    const row = {
      sekolah_data_semester_id,
      kepsek_id: await getKepsekId(),
      master_kurikulum_id: await getMasterKurikulumId(content.profil.kurikulum),
      akreditasi: content.profil.akreditasi,
      status_kepemilikan: content.profil.identitas_sekolah.status_kepemilikan,
      sk_pendirian: content.profil.identitas_sekolah.sk_pendirian_sekolah,
      tgl_pendirian: content.profil.identitas_sekolah.tanggal_sk_pendirian,
      sk_izin_operasional: content.profil.identitas_sekolah.sk_izin_operasional,
      tgl_sk_izin_operasional: content.profil.identitas_sekolah.tanggal_sk_izin_operasional,
      daya_listrik: content.profil.data_rinci.daya_listrik,
      status_bos: content.profil.data_rinci.status_bos,
      sertifikat_iso: content.profil.data_rinci.sertifikasi_iso,
      operator_id: content.profil.identitas_sekolah.operator,
      tgl_insert: new Date(),
    }
    rows.push(row)
  }

  const model = sequelize.import('../models/sekolah_data_profil')
  for(let row of rows) {
    const where = {
      sekolah_data_semester_id: row.sekolah_data_semester_id
    }

    const isExist = Boolean((await model.findAll({
      attributes: [[sequelize.fn('COUNT', sequelize.col('id')), 'count']],
      where
    }))[0].dataValues.count)
    if(isExist) continue

    await model.create(row, { ignoreDuplicates: true })
  }
}

module.exports.importData = importData
