const config = require('../config/config')
const { Sequelize, sequelize } = require('../models')
const glob = require('glob-promise')
const util = require('util')

const importData = async () => {
  const kabupatenFiles = await glob(config.dataPath + '/1-*')
  var rows = []

  for(let file of kabupatenFiles) {
    const content = require('../' + file)
    for(let line of content) {
      const row = {
        id: parseInt(line.kode_wilayah),
        nama_kabupaten: line.nama,
        master_provinsi_id: line.kode_wilayah_induk_provinsi
      }
      rows.push(row)
    }
  }

  const kabupaten = sequelize.import('../models/master_kabupaten')
  await kabupaten.bulkCreate(rows, { ignoreDuplicates: true })
}

module.exports.importData = importData