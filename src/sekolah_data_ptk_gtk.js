const config = require('../config/config')
const { Sequelize, sequelize } = require('../models')
const glob = require('glob-promise')
const util = require('util')
const _ = require('lodash')
const getSekolahDataSemesterId = require('./helper/get_sekolah_data_semester_id')
const getPtkGtKId = require('./helper/get_ptk_gtk_id')

const importData = async () => {
  const files = await glob(config.dataPath + '/**/getGTK.json')
  var rows = []

  for(let file of files) {
    const content = require('../' + file)
    const sekolah_data_semester_id = await getSekolahDataSemesterId(file)
    
    for(let line of content.rows) {
      const master_ptk_gtk_id = await getPtkGtKId(line.ptk_id)

      const row = {
        master_ptk_gtk_id,
        master_jenis_ptk_id: line.jenis_ptk_id,
        sekolah_data_semester: sekolah_data_semester_id,
        master_status_kepegawaian_id: line.status_kepegawaian_id,
        tgl_surat_tugas: line.tanggal_surat_tugas,
        tgl_insert: new Date()
      }

      rows.push(row)
    }
  }

  const model = sequelize.import('../models/sekolah_data_ptk_gtk')
  for(let row of rows) {
    const where = {
      sekolah_data_semester: row.sekolah_data_semester
    }

    const isExist = Boolean((await model.findAll({
      attributes: [[sequelize.fn('COUNT', sequelize.col('id')), 'count']],
      where
    }))[0].dataValues.count)
    if(isExist) continue

    await model.create(row, { ignoreDuplicates: true })
  }
}

module.exports.importData = importData