const config = require('../config/config')
const { Sequelize, sequelize } = require('../models')
const glob = require('glob-promise')
const util = require('util')
const _ = require('lodash')
const getPesertaDidikId = require('./helper/get_master_peserta_didik_id_by_peserta_didik_id')
const path = require('path')

const TIDAK_AKTIF = 0
const AKTIF = 1

const importData = async () => {
  const files = await glob(config.dataPath + '/**/getPesertaDidik.json')
  var rows = []

  for(let file of files) {
    const content = require('../' + file)
    const profilSekolah = require('../' + path.dirname(file) + '/profil.json')
    for(let line of content.rows) {
      const peserta_didik_id = await getPesertaDidikId(line.peserta_didik_id)

      const row = {
        master_peserta_didik_id: peserta_didik_id,
        master_jenis_pendaftaran_id: line.jenis_pendaftaran_id,
        tgl_masuk: line.tanggal_masuk_sekolah,
        status_peserta_didik: TIDAK_AKTIF, // TODO
        npsn: profilSekolah.profil.identitas_sekolah.npsn,
        tgl_insert: new Date(),
      }
      rows.push(row)
    }
  }

  const model = sequelize.import('../models/sekolah_data_peserta_didik_register')
  await model.bulkCreate(rows, { ignoreDuplicates: true })
}

module.exports.importData = importData