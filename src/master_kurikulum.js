const config = require('../config/config')
const { Sequelize, sequelize } = require('../models')
const glob = require('glob-promise')
const util = require('util')
const _ = require('lodash')

const importData = async () => {
  const files = await glob(config.dataPath + '/**/getRombel.json')
  var rows = []
  var idCounter = 1

  for(let file of files) {
    const content = require('../' + file)
    for(let line of content.rows) {
      const row = {
        id: line.kurikulum_id,
        nama_kurikulum: line.kurikulum_id_str
      }
      rows.push(row)
    }
  }

  rows = _.uniqBy(rows, 'nama_kurikulum')
  const model = sequelize.import('../models/master_kurikulum')
  await model.bulkCreate(rows, { ignoreDuplicates: true })
}

module.exports.importData = importData