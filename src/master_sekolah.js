const config = require('../config/config')
const { Sequelize, sequelize } = require('../models')
const glob = require('glob-promise')
const util = require('util')
const getKecamatanIdByKodeWilayahInduk = require('./helper/get_kecamatan_id_by_kode_wilayah_induk')
const getBentukPendidikanIdByNama = require('./helper/get_bentuk_pendidikan_id_by_nama')
const getProfilByNPSN = require('./helper/get_profil_by_npsn')
const getKontakByNPSN = require('./helper/get_kontak_by_npsn')

const importData = async () => {
  const files = await glob(config.dataPath + '/+(sd|slb|smp|sma|smk)-*')
  var rows = []

  for(let file of files) {
    const content = require('../' + file)
    for(let line of content) {
      //const npsn = 20300158 // test
      const npsn = line.npsn.toString()
      const kecamatanId = await getKecamatanIdByKodeWilayahInduk(parseInt(line.kode_wilayah_induk_kecamatan))
      const bentukPendidikanId = await getBentukPendidikanIdByNama(line.bentuk_pendidikan)
      const statusSekolah = (line.status_sekolah === 'Negeri' ? 1 : 2)
      const profil = getProfilByNPSN(npsn)
      const kontak = getKontakByNPSN(npsn)
      const rt = kontak.kontak_utama['rt_/_rw'].split(' / ')[0]
      const rw = kontak.kontak_utama['rt_/_rw'].split(' / ')[1]
      const koordinat = (kontak.kontak_utama.bujur ? kontak.kontak_utama.bujur + ', ' + kontak.kontak_utama.lintang : null)

      const row = {
        nama_sekolah: line.nama,
        master_provinsi_id: line.kode_wilayah_induk_provinsi,
        npsn: npsn,
        nama: line.nama,
        sekolah_id: line.sekolah_id,
        sekolah_id_enkripsi: line.sekolah_id_enkrip,
        master_kecamatan_id: kecamatanId,
        master_bentuk_pendidikan_id: bentukPendidikanId,
        status_sekolah: statusSekolah,
        waktu_penyelanggaraan: profil.data_rinci.waku_penyelenggaraan,
        desa: kontak.kontak_utama['desa_/_kelurahan'],
        dusun: kontak.kontak_utama.dusun,
        rt,
        rw,
        koordinat,
        sumber_listrik: profil.data_rinci.sumber_listrik,
        luas_tanah: profil.data_lengkap.luas_tanah_milik + profil.data_lengkap.luas_tanah_bukan_milik,
        akses_internet: profil.data_rinci.akses_internet,
      }

      rows.push(row)
    }
  }

  const model = sequelize.import('../models/master_sekolah')
  for(let row of rows) {
    const where = { npsn: row.npsn }

    const isExist = Boolean((await model.findAll({
      attributes: [[sequelize.fn('COUNT', sequelize.col('id')), 'count']],
      where
    }))[0].dataValues.count)
    if(isExist) continue

    await model.create(row, { ignoreDuplicates: true })
  }
}

module.exports.importData = importData
