const config = require('../config/config')
const { Sequelize, sequelize } = require('../models')
const glob = require('glob-promise')
const util = require('util')
const getKecamatanIdByKodeWilayahInduk = require('./helper/get_kecamatan_id_by_kode_wilayah_induk')
const getBentukPendidikanIdByNama = require('./helper/get_bentuk_pendidikan_id_by_nama')
const getProfilByNPSN = require('./helper/get_profil_by_npsn')
const getKontakByNPSN = require('./helper/get_kontak_by_npsn')

const importData = async () => {
  const files = await glob(config.dataPath + '/**/rekapitulasi.json')
  return
}

module.exports.importData = importData