const config = require('../config/config')
const { Sequelize, sequelize } = require('../models')
const glob = require('glob-promise')
const util = require('util')
const _ = require('lodash')

const importData = async () => {
  const files = await glob(config.dataPath + '/**/getGTK.json')
  var rows = []
  var idCounter = 1

  for(let file of files) {
    const content = require('../' + file)
    for(let line of content.rows) {
      const row = {
        id: line.jenis_ptk_id,
        nama_jenis_ptk: line.jenis_ptk_id_string
      }
      rows.push(row)
    }
  }

  rows = _.uniqBy(rows, 'nama_jenis_ptk')
  const model = sequelize.import('../models/master_jenis_ptk')
  for(let row of rows) {
    const where = { nama_jenis_ptk: row.nama_jenis_ptk }

    const isExist = Boolean((await model.findAll({
      attributes: [[sequelize.fn('COUNT', sequelize.col('id')), 'count']],
      where
    }))[0].dataValues.count)
    if(isExist) continue

    await model.create(row, { ignoreDuplicates: true })
  }
}

module.exports.importData = importData