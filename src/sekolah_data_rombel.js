const config = require('../config/config')
const { Sequelize, sequelize } = require('../models')
const glob = require('glob-promise')
const util = require('util')
const _ = require('lodash')
const getSemesterId = require('./helper/get_semester_id')
const getWalasId = require('./helper/get_walas_id')

const importData = async () => {
  const files = await glob(config.dataPath + '/**/getRombel.json')
  var rows = []
  var idCounter = 1

  for(let file of files) {
    const content = require('../' + file)
    for(let line of content.rows) {
      const sekolah_data_semester_id = await getSemesterId(line.semester_id)
      const sekolah_data_walas_id = await getWalasId(line.ptk_id)

      const row = {
        rombongan_belajar_id: line.rombongan_belajar_id,
        nama_rombel: line.nama,
        master_tingkat_id: line.tingkat_pendidikan_id,
        master_jurusan_id: parseInt(line.jurusan_id),
        sekolah_data_walas_id,
        sekolah_data_semester_id,
        tgl_insert: new Date(),
      }
      rows.push(row)
    }
  }

  rows = _.uniqBy(rows, 'rombongan_belajar_id')
  const model = sequelize.import('../models/sekolah_data_rombel')
  for(let row of rows) {
    const where = {
      rombongan_belajar_id: row.rombongan_belajar_id
    }

    const isExist = Boolean((await model.findAll({
      attributes: [[sequelize.fn('COUNT', sequelize.col('id')), 'count']],
      where
    }))[0].dataValues.count)
    if(isExist) continue

    await model.create(row, { ignoreDuplicates: true })
  }
}

module.exports.importData = importData