const config = require('../config/config')
const { Sequelize, sequelize } = require('../models')
const glob = require('glob-promise')
const util = require('util')
const _ = require('lodash')

const importData = async () => {
  const files = await glob(config.dataPath + '/**/getPesertaDidik.json')
  var rows = []

  for(let file of files) {
    const content = require('../' + file)
    for(let line of content.rows) {
      const row = {
        id: line.jenis_pendaftaran_id,
        nama_jenis_pendaftaran: line.jenis_pendaftaran_id_string
      }
      rows.push(row)
    }
  }

  rows = _.uniqBy(rows, 'id')
  rows = _.uniqBy(rows, 'nama_jenis_pendaftaran')
  const model = sequelize.import('../models/master_jenis_pendaftaran')
  for(let row of rows) {
    const where = { nama_jenis_pendaftaran: row.nama_jenis_pendaftaran }

    const isExist = Boolean((await model.findAll({
      attributes: [[sequelize.fn('COUNT', sequelize.col('id')), 'count']],
      where
    }))[0].dataValues.count)
    if(isExist) continue

    await model.create(row, { ignoreDuplicates: true })
  }
}

module.exports.importData = importData