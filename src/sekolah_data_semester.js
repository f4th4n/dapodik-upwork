const config = require('../config/config')
const { Sequelize, sequelize } = require('../models')
const glob = require('glob-promise')
const util = require('util')
const _ = require('lodash')
const master_sekolah = sequelize.import('../models/master_sekolah')
const master_semester = sequelize.import('../models/master_semester')

const TIDAK_AKTIF = 0
const AKTIF = 1

const importData = async () => {
  const rowsSekolah = await master_sekolah.findAll()
  const rowsSemester = await master_semester.findAll()

  var rows = []
  var idCounter = 1

  const getStatus = (rowSemester) => {
    const semesterGanjil = (rowSemester.nama_semester === 0 ? 'genap' : 'ganjil')
    return (semesterGanjil === config.current_semester) && rowSemester.tahun_pelajaran == config.current_tahun_ajaran
  }

  for(let rowSekolah of rowsSekolah) {
    for(let rowSemester of rowsSemester) {
      const row = {
        id: idCounter++,
        master_sekolah_id: rowSekolah.id,
        master_semester_id: rowSemester.id,
        status: getStatus(rowSemester),
        tgl_insert: new Date()
      }
      rows.push(row)
    }
  }

  const model = sequelize.import('../models/sekolah_data_semester')
  for(let row of rows) {
    const where = {
      master_sekolah_id: row.master_sekolah_id,
      master_semester_id: row.master_semester_id,
      status: row.status,
      tgl_insert: row.tgl_insert
    }

    const isExist = Boolean((await model.findAll({
      attributes: [[sequelize.fn('COUNT', sequelize.col('id')), 'count']],
      where
    }))[0].dataValues.count)
    if(isExist) continue

    await model.create(row, { ignoreDuplicates: true })
  }
}

module.exports.importData = importData