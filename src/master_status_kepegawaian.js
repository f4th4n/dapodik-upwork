const config = require('../config/config')
const { Sequelize, sequelize } = require('../models')
const glob = require('glob-promise')
const util = require('util')
const _ = require('lodash')

const importData = async () => {
  const files = await glob(config.dataPath + '/**/getGTK.json')
  var rows = []
  var idCounter = 1

  for(let file of files) {
    const content = require('../' + file)
    for(let line of content.rows) {
      const row = {
        id: line.status_kepegawaian_id,
        nama_status_kepegawaian: line.status_kepegawaian_id_str
      }
      rows.push(row)
    }
  }

  rows = _.uniqBy(rows, 'nama_status_kepegawaian')
  const model = sequelize.import('../models/master_status_kepegawaian')
  for(let row of rows) {
    const where = { nama_status_kepegawaian: row.nama_status_kepegawaian }

    const isExist = Boolean((await model.findAll({
      attributes: [[sequelize.fn('COUNT', sequelize.col('id')), 'count']],
      where
    }))[0].dataValues.count)
    if(isExist) continue

    await model.create(row, { ignoreDuplicates: true })
  }
}

module.exports.importData = importData