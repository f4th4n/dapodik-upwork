const config = require('../config/config')
const { Sequelize, sequelize } = require('../models')
const glob = require('glob-promise')
const util = require('util')
const _ = require('lodash')

const importData = async () => {
  const files = await glob(config.dataPath + '/+(sd|slb|smp|sma|smk)-*')
  var rows = []

  for(let file of files) {
    const content = require('../' + file)
    for(let line of content) {
      const row = {
        nama_bentuk_pendidikan: line.bentuk_pendidikan
      }
      rows.push(row)
    }
  }

  rows = _.uniqBy(rows, 'nama_bentuk_pendidikan')
  const model = sequelize.import('../models/master_bentuk_pendidikan')
  for(let row of rows) {
    const where = { nama_bentuk_pendidikan: row.nama_bentuk_pendidikan }

    const isExist = Boolean((await model.findAll({
      attributes: [[sequelize.fn('COUNT', sequelize.col('id')), 'count']],
      where
    }))[0].dataValues.count)
    if(isExist) continue

    await model.create(row, { ignoreDuplicates: true })
  }
}

module.exports.importData = importData