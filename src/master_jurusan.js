const config = require('../config/config')
const { Sequelize, sequelize } = require('../models')
const glob = require('glob-promise')
const util = require('util')
const _ = require('lodash')

const importData = async () => {
  const files = await glob(config.dataPath + '/**/getRombel.json')
  var rows = []
  var idCounter = 1

  for(let file of files) {
    const content = require('../' + file)
    for(let line of content.rows) {
      const row = {
        id: line.jurusan_id,
        nama_jurusan: line.jurusan_id_string
      }
      rows.push(row)
    }
  }

  rows = _.uniqBy(rows, 'nama_jurusan')
  const model = sequelize.import('../models/master_jurusan')
  for(let row of rows) {
    const where = { nama_jurusan: row.nama_jurusan }

    const isExist = Boolean((await model.findAll({
      attributes: [[sequelize.fn('COUNT', sequelize.col('id')), 'count']],
      where
    }))[0].dataValues.count)
    if(isExist) continue

    await model.create(row, { ignoreDuplicates: true })
  }
}

module.exports.importData = importData