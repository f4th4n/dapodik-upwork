const config = require('../config/config')
const { Sequelize, sequelize } = require('../models')
const glob = require('glob-promise')
const util = require('util')
const _ = require('lodash')

const GANJIL = 0
const GENAP = 1

const importData = async () => {
  var rows = []
  var idCounter = 1

  const tahunIniPlus2 = (new Date().getFullYear()) + 2
  for(let tahun = 2019; tahun < tahunIniPlus2; tahun++) {
    let tahun_pelajaran = tahun.toString() + '/' + (tahun + 1).toString()
    rows.push({ id: idCounter, nama_semester: GANJIL, tahun_pelajaran })
    idCounter++
    rows.push({ id: idCounter, nama_semester: GENAP, tahun_pelajaran })
    idCounter++
  }

  const model = sequelize.import('../models/master_semester')
  await model.bulkCreate(rows, { ignoreDuplicates: true })
}

module.exports.importData = importData