const config = require('../config/config')
const { Sequelize, sequelize } = require('../models')
const glob = require('glob-promise')
const util = require('util')
const _ = require('lodash')
const getKecamatanIdByKodeWilayahInduk = require('./helper/get_kecamatan_id_by_kode_wilayah_induk')
const getBentukPendidikanIdByNama = require('./helper/get_bentuk_pendidikan_id_by_nama')
const getProfilByNPSN = require('./helper/get_profil_by_npsn')
const getKontakByNPSN = require('./helper/get_kontak_by_npsn')

const importData = async () => {
  const files = await glob(config.dataPath + '/**/getPesertaDidik.json')
  var rows = []

  for(let file of files) {
    const content = require('../' + file)
    for(let line of content.rows) {
      const row = {
        id: line.tingkat_pendidikan_id,
        nama_tingkat: line.tingkat_pendidikan_id
      }

      rows.push(row)
    }
  }

  rows = _.uniqBy(rows, 'nama_tingkat')

  const model = sequelize.import('../models/master_tingkat')
  for(let row of rows) {
    const where = { nama_tingkat: row.nama_tingkat }

    const isExist = Boolean((await model.findAll({
      attributes: [[sequelize.fn('COUNT', sequelize.col('id')), 'count']],
      where
    }))[0].dataValues.count)
    if(isExist) continue

    await model.create(row, { ignoreDuplicates: true })
  }
}

module.exports.importData = importData