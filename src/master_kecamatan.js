const config = require('../config/config')
const { Sequelize, sequelize } = require('../models')
const glob = require('glob-promise')
const util = require('util')

const importData = async () => {
  const kecamatanFiles = await glob(config.dataPath + '/2-*')
  var rows = []

  for(let file of kecamatanFiles) {
    const content = require('../' + file)
    for(let line of content) {
      const row = {
        id: parseInt(line.kode_wilayah),
        nama_kecamatan: line.nama,
        master_kabupaten_id: line.kode_wilayah_induk_kabupaten
      }
      rows.push(row)
    }
  }

  const kecamatan = sequelize.import('../models/master_kecamatan')
  await kecamatan.bulkCreate(rows, { ignoreDuplicates: true })
}

module.exports.importData = importData