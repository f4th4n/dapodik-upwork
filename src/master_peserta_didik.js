const config = require('../config/config')
const { Sequelize, sequelize } = require('../models')
const glob = require('glob-promise')
const util = require('util')
const _ = require('lodash')

const importData = async () => {
  const files = await glob(config.dataPath + '/**/getPesertaDidik.json')
  var rows = []
  var idCounter = 1

  for(let file of files) {
    const content = require('../' + file)
    for(let line of content.rows) {
      const row = {
        peserta_didik_id: line.peserta_didik_id,
        nama: line.nama,
        nisn: line.nisn,
        nipd: line.nipd,
        jenis_kelamin: line.jenis_kelamin,
        tempat_lahir: line.tempat_lahir,
        tgl_lahir: line.tanggal_lahir,
        agama_id: line.agama_id
      }
      rows.push(row)
    }
  }

  rows = _.uniqBy(rows, 'peserta_didik_id')
  const model = sequelize.import('../models/master_peserta_didik')
  for(let row of rows) {
    const where = { peserta_didik_id: row.peserta_didik_id }

    const isExist = Boolean((await model.findAll({
      attributes: [[sequelize.fn('COUNT', sequelize.col('id')), 'count']],
      where
    }))[0].dataValues.count)
    if(isExist) continue

    await model.create(row, { ignoreDuplicates: true })
  }
}

module.exports.importData = importData