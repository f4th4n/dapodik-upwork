const { Sequelize, sequelize } = require('../../models')

var rowsCache = []

const GANJIL = 0
const GENAP  = 1

// argId=20191|20192|20201
const getSemesterId = async (argId) => {
  const row = sequelize.import('../../models/master_semester')
  if(!rowsCache.length) {
    rowsCache = await row.findAll()
  }

  const tahun = parseInt(argId.slice(0, 4))
  const tahunPelajaran = tahun.toString() + '/' + (tahun + 1).toString()
  const semester = (argId.slice(4, 5) == '1' ? GANJIL : GENAP)
  const rows = rowsCache.filter(row => row.tahun_pelajaran == tahunPelajaran && row.nama_semester == semester)
  if(!rows.length) return null

  return rows[0].id
}

module.exports = getSemesterId