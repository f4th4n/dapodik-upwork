const { Sequelize, sequelize } = require('../../models')
const config = require('../../config/config')
const MasterSekolah = sequelize.import('../../models/master_sekolah')
const MasterSemester = sequelize.import('../../models/master_semester')
const SekolahDataSemester = sequelize.import('../../models/sekolah_data_semester')
const path = require('path')

const getMasterSekolahId = async (file) => {
  const npsn = path.basename(path.dirname(file))
  const row = await MasterSekolah.findOne({ where: {npsn} })
  return row ? row.id : null
}

const getMasterSemesterId = async () => {
  const nama_semester = (config.current_semester === 'ganjil' ? 1 : 0)
  const tahun_pelajaran = config.current_tahun_ajaran
  const row = await MasterSemester.findOne({ where: {nama_semester, tahun_pelajaran} })
  return row ? row.id : null
}

const getSekolahDataSemesterId = async (file) => {
  const masterSekolahId = await getMasterSekolahId(file)
  const masterSemesterId = await getMasterSemesterId()
  const row = await SekolahDataSemester.findOne({ where: { master_sekolah_id: masterSekolahId, master_semester_id: masterSemesterId } })
  return row ? row.id : null
}

module.exports = getSekolahDataSemesterId