const { Sequelize, sequelize } = require('../../models')

var rowsCache = []

const getKecamatanIdByKodeWilayahInduk = async (kode_wilayah_induk_kecamatan) => {
  const kecamatan = sequelize.import('../../models/master_kecamatan')
  if(!rowsCache.length) {
    rowsCache = await kecamatan.findAll()
  }

  const rows = rowsCache.filter(kec => kec.id == kode_wilayah_induk_kecamatan)
  if(!rows.length) return null

  return rows[0].id
}

module.exports = getKecamatanIdByKodeWilayahInduk