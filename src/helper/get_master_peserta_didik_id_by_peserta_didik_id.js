const { Sequelize, sequelize } = require('../../models')

var rowsCache = []

const getBentukPendidikanIdByNama = async (argId) => {
  const row = sequelize.import('../../models/master_peserta_didik')
  if(!rowsCache.length) {
    rowsCache = await row.findAll()
  }

  const rows = rowsCache.filter(row => row.peserta_didik_id == argId)
  if(!rows.length) return null

  return rows[0].id
}

module.exports = getBentukPendidikanIdByNama