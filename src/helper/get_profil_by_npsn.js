const config = require('../../config/config')

var rowsCache = []

const getProfilByNPSN = (npsn) => {
  try {
    return require(`../../${ config.dataPath }/${ npsn }/profil`).profil
  } catch(e) {
    return {
      "identitas_sekolah": {
        "bentuk_pendidikan": null,
        "npsn": null,
        "sk_pendirian_sekolah": null,
        "status_kepemilikan": null,
        "tanggal_sk_pendirian": null,
        "tanggal_sk_izin_operasional": null,
        "status": null,
        "sk_izin_operasional": null
      },
      "waktu": null,
      "akreditasi": null,
      "data_rinci": {
        "sumber_listrik": null,
        "waku_penyelenggaraan": null,
        "status_bos": null,
        "sertifikasi_iso": null,
        "akses_internet": null,
        "daya_listrik": null
      },
      "operator": null,
      "data_lengkap": {
        "cabang_kcp/unit": null,
        "luas_tanah_milik": null,
        "rekening_atas_nama": null,
        "luas_tanah_bukan_milik": null,
        "nama_bank": null,
        "kebutuhan_khusus_dilayani": null
      },
      "kepsek": null,
      "kurikulum": null
    }
  }
}

module.exports = getProfilByNPSN
