const config = require('../../config/config')

var rowsCache = []

const getKontakByNPSN = (npsn) => {
  try {
    return require(`../../${ config.dataPath }/${ npsn }/kontak`).kontak
  } catch(e) {
    return {
      "kontak_utama": {
        "bujur": null,
        "lintang": null,
        "kode_pos": null,
        "rt_/_rw": '0 / 0',
        "provinsi": null,
        "alamat": null,
        "kabupaten": null,
        "kecamatan": null,
        "dusun": null,
        "desa_/_kelurahan": null
      }
    }
  }
}

module.exports = getKontakByNPSN