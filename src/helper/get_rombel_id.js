const { Sequelize, sequelize } = require('../../models')

var rowsCache = []

const func = async (rombongan_belajar_id) => {
  const model = sequelize.import('../../models/sekolah_data_rombel')
  if(!rowsCache.length) {
    rowsCache = await model.findAll()
  }

  const rows = rowsCache.filter(row => row.rombongan_belajar_id == rombongan_belajar_id)
  if(!rows.length) return null

  return rows[0].id
}

module.exports = func