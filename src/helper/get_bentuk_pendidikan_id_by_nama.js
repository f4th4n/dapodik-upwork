const { Sequelize, sequelize } = require('../../models')

var rowsCache = []

const getBentukPendidikanIdByNama = async (nama) => {
  const row = sequelize.import('../../models/master_bentuk_pendidikan')
  if(!rowsCache.length) {
    rowsCache = await row.findAll()
  }

  const rows = rowsCache.filter(row => row.nama_bentuk_pendidikan == nama)
  if(!rows.length) return null

  return rows[0].id
}

module.exports = getBentukPendidikanIdByNama