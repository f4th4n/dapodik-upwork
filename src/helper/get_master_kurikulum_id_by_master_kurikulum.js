const { Sequelize, sequelize } = require('../../models')

const func = async (nama_kurikulum) => {
  const model = sequelize.import('../../models/master_kurikulum')
  var row = await model.findOne({ where: { nama_kurikulum } })
  if(!row) {
    await model.create({ nama_kurikulum })
  }
  return row
}

module.exports = func
