const { Sequelize, sequelize } = require('../../models')

var rowsCache = []

const func = async (ptk_id) => {
  const model = sequelize.import('../../models/master_ptk_gtk')
  if(!rowsCache.length) {
    rowsCache = await model.findAll()
  }

  const rows = rowsCache.filter(row => row.ptk_id == ptk_id)
  if(!rows.length) return null

  return rows[0].id
}

module.exports = func